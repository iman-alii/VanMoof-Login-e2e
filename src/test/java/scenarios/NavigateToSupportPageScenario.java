package scenarios;
import okhttp3.Address;
import org.apache.hc.core5.util.Asserts;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import static junit.framework.TestCase.assertEquals;

public class NavigateToSupportPageScenario extends  LoginPageScenarioBase {

	@Test
	public void NavigateToSupportPage () {

		//act
		WebElement submitButtonElement =  loginPage.GetElementBy(loginPage.supportBtn);
		submitButtonElement.click();

		//Navigate to the new tab
		loginPage.NavigateToNextTab();

		//wait for the page title to be displayed
		loginPage.WaitForElement(loginPage.supportPageTitle);

		//Make sure user is properly Navigated to Support page
		String ActualPageUrl = loginPage.GetUrl();
		String ExpectedPageUrl = "https://www.vanmoof.com/en-NL/contact-us";
		Assert.assertEquals(ExpectedPageUrl, ActualPageUrl);

		//Assert Page title is displayed "How Can we help ?"
		String actualValidationMessage = loginPage.GetElementBy(loginPage.supportPageTitle).getText();
		Assert.assertEquals("How can we help?", actualValidationMessage);

		//Assert on the page Title
		String ActualPageTitle = loginPage.GetPageTitle();
		Assert.assertEquals("VanMoof Support & Contact | VanMoof", ActualPageTitle);


	}
}