package scenarios;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;

public class NavigateToForgotYourPasswordPageScenario extends  LoginPageScenarioBase {

	@Test
	public void NavigateToForgotYourPasswordPage () {

		//act
		WebElement submitButtonElement =  loginPage.GetElementBy(loginPage.forgotPasswordBtn);
		submitButtonElement.click();

		//wait for the page title to be displayed
		loginPage.WaitForElement(loginPage.resetPasswordTitle);

		//Make sure user is properly Navigated to Reset Password page
		String ActualPageUrl = loginPage.GetUrl();
		String ExpectedPageUrl = "https://www.vanmoof.com/my-vanmoof/password/email";
		Assert.assertEquals(ExpectedPageUrl, ActualPageUrl);

		//Assert Page title is displayed "Reset Password"
		String actualValidationMessage = loginPage.GetElementBy(loginPage.resetPasswordTitle).getText();
		Assert.assertEquals("Reset password", actualValidationMessage);
	}
}