package scenarios;

import org.junit.After;
import org.junit.Before;
import pages.LoginPage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class LoginPageScenarioBase {

   protected LoginPage loginPage;
    @Before
    public void setup()   {
        loginPage = new LoginPage();
    }

    @After
    public void cleanup() {
        loginPage.cleanup();
    }

    }
