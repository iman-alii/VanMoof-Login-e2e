package scenarios;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import java.io.Console;


public class LoginWithMissingEmailAndPasswordScenario extends  LoginPageScenarioBase {

	@Test
	public void LoginWithMissingEmailAndPassword () {

		String generatedString = RandomString.make(5);
		//arrange
		WebElement emailElement =  loginPage.GetElementBy(loginPage.emailField);
		emailElement.sendKeys("");

		WebElement passwordElement =  loginPage.GetElementBy(loginPage.passwordField);
		passwordElement.sendKeys("");

		//act
		WebElement submitButtonElement =  loginPage.GetElementBy(loginPage.loginBtn);
		submitButtonElement.click();

		//Make sure user is not navigated
		String ActualPageUrl = loginPage.GetUrl();
		String ExpectedPageTitle = "https://www.vanmoof.com/my-vanmoof/auth/login";
		Assert.assertEquals(ExpectedPageTitle, ActualPageUrl);

		//Assert the validation message appears
		loginPage.VerifyTextPresent("The Email field is required.");
		loginPage.VerifyTextPresent("TThe Password field is required.");


	}
}