package scenarios;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;

public class NavigateToCreateAccountPageScenario extends  LoginPageScenarioBase {

	@Test
	public void NavigateToCreateAccountPage () {

		//act
		WebElement submitButtonElement =  loginPage.GetElementBy(loginPage.createNewAccountBtn);
		submitButtonElement.click();

		//wait for the page title to be displayed
		loginPage.WaitForElement(loginPage.createNewAccountTitle);

		//Make sure user is properly Navigated to registration page
		String ActualPageUrl = loginPage.GetUrl();
		String ExpectedPageUrl = "https://www.vanmoof.com/my-vanmoof/auth/register";
		Assert.assertEquals(ExpectedPageUrl, ActualPageUrl);

		//Assert Page title is displayed "Create Account"
		String actualValidationMessage = loginPage.GetElementBy(loginPage.createNewAccountTitle).getText();
		Assert.assertEquals("Create account", actualValidationMessage);
	}
}