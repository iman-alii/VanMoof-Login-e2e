package scenarios;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;

public class NavigateToBackToShopScenario extends  LoginPageScenarioBase {

	@Test
	public void NavigateToBackToShop () {

		//action
		WebElement submitButtonElement =  loginPage.GetElementBy(loginPage.backToShopBtn);
		submitButtonElement.click();

		//wait for the page title to be displayed
		loginPage.WaitForElement(loginPage.rideTheFutureTogetherTitle);

		//Make sure user is properly Navigated to main shop page
		String ActualPageUrl = loginPage.GetUrl();
		String ExpectedPageUrl = "https://www.vanmoof.com/en-NL";
		Assert.assertEquals(ExpectedPageUrl, ActualPageUrl);

		//Assert Page title is displayed "Ride the future together"
		String actualValidationMessage = loginPage.GetElementBy(loginPage.rideTheFutureTogetherTitle).getText();
		Assert.assertEquals("Ride the future together", actualValidationMessage);

		//Assert on the page Title
		String ActualPageTitle = loginPage.GetPageTitle();
		Assert.assertEquals("Ride the future with our Electric Bikes | VanMoof", ActualPageTitle);
	}
}