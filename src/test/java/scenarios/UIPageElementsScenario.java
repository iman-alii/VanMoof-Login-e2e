package scenarios;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;

public class UIPageElementsScenario extends  LoginPageScenarioBase {

	@Test
	public void UIPageElements () {
		//check e-mail field is displayed
		WebElement emailField =  loginPage.GetElementBy(loginPage.emailField);
		emailField.isDisplayed();

		//check password field is displayed
		WebElement passwordField =  loginPage.GetElementBy(loginPage.passwordField);
		passwordField.isDisplayed();

		//check forgot password link is displayed and text is correct
		WebElement forgotPasswordBtn =  loginPage.GetElementBy(loginPage.forgotPasswordBtn);
		forgotPasswordBtn.isDisplayed();
		String forgotPasswordActualText = loginPage.GetElementBy(loginPage.forgotPasswordBtn).getText();
		Assert.assertEquals("Forgot your password?", forgotPasswordActualText);

		//check Login Button is displayed
		WebElement loginBtn =  loginPage.GetElementBy(loginPage.loginBtn);
		loginBtn.isDisplayed();

		//check create New Account Button is displayed and text is correct
		WebElement createNewAccountBtn =  loginPage.GetElementBy(loginPage.createNewAccountBtn);
		createNewAccountBtn.isDisplayed();
		String actualTextCreateNewAccount = loginPage.GetElementBy(loginPage.createNewAccountBtn).getText();
		Assert.assertEquals("Create an account", actualTextCreateNewAccount);

		//check Back to Shop Button is displayed and text is correct
		WebElement backToShopBtn =  loginPage.GetElementBy(loginPage.backToShopBtn);
		backToShopBtn.isDisplayed();
		String actualTextToShop = loginPage.GetElementBy(loginPage.backToShopBtn).getText();
		Assert.assertEquals("Back to shop", actualTextToShop);

		//check New Here? Label is displayed and text is correct
		WebElement newHereLabel =  loginPage.GetElementBy(loginPage.newHereLabel);
		newHereLabel.isDisplayed();
		String actualTextNewHere = loginPage.GetElementBy(loginPage.newHereLabel).getText();
		Assert.assertEquals("New here?", actualTextNewHere);

		//check With a VanMoof account you can: Label is displayed and text is correct
		WebElement withVanMoofAccountLabel =  loginPage.GetElementBy(loginPage.withVanMoofAccountLabel);
		withVanMoofAccountLabel.isDisplayed();
		String actualTextWithVanMoofAccount = loginPage.GetElementBy(loginPage.withVanMoofAccountLabel).getText();
		Assert.assertEquals("With a VanMoof account you can:", actualTextWithVanMoofAccount);

		//check Support Button is displayed and text is correct
		WebElement supportBtn =  loginPage.GetElementBy(loginPage.supportBtn);
		supportBtn.isDisplayed();
		String actualTextSupportBtn = loginPage.GetElementBy(loginPage.supportBtn).getText();
		Assert.assertEquals("Support", actualTextSupportBtn);

		//check Track Your Order Label is displayed and text is correct
		WebElement trackYourOrderLabel =  loginPage.GetElementBy(loginPage.trackYourOrderLabel);
		trackYourOrderLabel.isDisplayed();
		String actualTextTrackYourOrder = loginPage.GetElementBy(loginPage.trackYourOrderLabel).getText();
		Assert.assertEquals("Track your order", actualTextTrackYourOrder);

		//check With language Button is displayed and text is correct
		WebElement languageBtn =  loginPage.GetElementBy(loginPage.languageBtn);
		languageBtn.isDisplayed();
		String actualTextLanguageBtn = loginPage.GetElementBy(loginPage.languageBtn).getText();
		Assert.assertEquals("English", actualTextLanguageBtn);

		//check Login button at the top Bar is displayed and text is correct
		WebElement barLoginBtn =  loginPage.GetElementBy(loginPage.barLoginBtn);
		barLoginBtn.isDisplayed();
		String actualTextBarLoginBtn = loginPage.GetElementBy(loginPage.barLoginBtn).getText();
		Assert.assertEquals("Login", actualTextBarLoginBtn);

		//check the Login Page Title and text is correct
		WebElement loginLabel =  loginPage.GetElementBy(loginPage.loginLabel);
		loginLabel.isDisplayed();
		String actualTextLoginTitle = loginPage.GetElementBy(loginPage.loginLabel).getText();
		Assert.assertEquals("Log in", actualTextLoginTitle);

		//check the sentence in the bottom of the page "Ride with VanMoof for the best price. Ever." is displayed and text is correct
		WebElement bottomSentence_RideWith =  loginPage.GetElementBy(loginPage.bottomSentenceLabel_RideWith);
		bottomSentence_RideWith.isDisplayed();
		String actualText = loginPage.GetElementBy(loginPage.bottomSentenceLabel_RideWith).getText();
		Assert.assertEquals("Ride with VanMoof for the best price. Ever.", actualText);

		//check the sentence in the bottom of the page AN OFFER YOU CAN’T REFUSE
		WebElement bottomSentence_Anoffer =  loginPage.GetElementBy(loginPage.bottomSentenceLabel_AnOFFER);
		bottomSentence_Anoffer.isDisplayed();
		String actualTextAnOFFER = loginPage.GetElementBy(loginPage.bottomSentenceLabel_AnOFFER).getText();
		Assert.assertEquals("AN OFFER YOU CAN’T REFUSE", actualTextAnOFFER);

		//check the Button Shop Outlet Sale
		WebElement shopOutletSaleBtn =  loginPage.GetElementBy(loginPage.shopOutletBtn);
		shopOutletSaleBtn.isDisplayed();
		String actualTextShopOutlet = loginPage.GetElementBy(loginPage.shopOutletBtn).getText();
		Assert.assertEquals("Shop Outlet Sale", actualTextShopOutlet);

		//check lable Book check-in and repair appointments with our Bike Doctors
		WebElement bookCheckinSentence =  loginPage.GetElementBy(loginPage.bookCheckinLabel);
		bookCheckinSentence.isDisplayed();
		String actualTextBookCheckin = loginPage.GetElementBy(loginPage.bookCheckinLabel).getText();
		Assert.assertEquals("Book check-in and repair appointments with our Bike Doctors", actualTextBookCheckin);

		//Assertion on the page URL
		String ActualPageUrl = loginPage.GetUrl();
		String ExpectedPageUrl = "https://www.vanmoof.com/my-vanmoof/auth/login";
		Assert.assertEquals(ExpectedPageUrl, ActualPageUrl);

		//Assertion on the Page Title
		String ActualPageTitle = loginPage.GetPageTitle();
		String ExpectedPageTitle = "My VanMoof | VanMoof";
		Assert.assertEquals(ExpectedPageTitle, ActualPageTitle);
	}
}