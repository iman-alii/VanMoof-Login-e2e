package scenarios;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LoginWithValidDataScenario extends  LoginPageScenarioBase {


	@Test
	public void LoginWithValidData () {

		//arrange

		WebElement emailElement =  loginPage.GetElementBy(loginPage.emailField);
		emailElement.sendKeys("iman.m.m.ali.a@gmail.com");

		WebElement passwordElement =  loginPage.GetElementBy(loginPage.passwordField);
		passwordElement.sendKeys("Xyzxyz1");

		//act
		WebElement submitButtonElement =  loginPage.GetElementBy(loginPage.loginBtn);
		submitButtonElement.click();

		//Wait till an item (Register a new Bike) in the home page appears
		loginPage.WaitForElement(loginPage.registerNewBikeBtn);

		//Make sure user is properly navigated to home page as a result of successful login
		String ActualPageUrl = loginPage.GetUrl();
		String ExpectedPageUrl = "https://www.vanmoof.com/my-vanmoof/home";
		Assert.assertEquals(ExpectedPageUrl, ActualPageUrl);
	}
}