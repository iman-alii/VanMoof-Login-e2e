package scenarios;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;


public class LoginWithInValidEmailScenario extends  LoginPageScenarioBase {

	@Test
	public void LoginWithInValidEmail () {

		//arrange
		String generatedString = RandomString.make(5);

		WebElement emailElement =  loginPage.GetElementBy(loginPage.emailField);
		emailElement.sendKeys(generatedString);

		WebElement passwordElement =  loginPage.GetElementBy(loginPage.passwordField);
		passwordElement.sendKeys(generatedString);

		//act
		WebElement submitButtonElement =  loginPage.GetElementBy(loginPage.loginBtn);
		submitButtonElement.click();

		//Make sure that user was not navigated
		String ActualPageUrl = loginPage.GetUrl();
		String ExpectedPageTitle = "https://www.vanmoof.com/my-vanmoof/auth/login";
		Assert.assertEquals(ExpectedPageTitle, ActualPageUrl);

		//Check validation message is displayed
		loginPage.VerifyTextPresent("Invalid email address.");
	}
}