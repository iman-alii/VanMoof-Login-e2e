package scenarios;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;


public class LoginWithInvalidDataScenario extends  LoginPageScenarioBase {

	@Test
	public void LoginWithInvalidData () {

		//arrange
		String generatedString = RandomString.make(5);
		
		WebElement emailElement =  loginPage.GetElementBy(loginPage.emailField);
		emailElement.sendKeys(generatedString+"@gmail.com");

		WebElement passwordElement =  loginPage.GetElementBy(loginPage.passwordField);
		passwordElement.sendKeys(generatedString);

		//act
		WebElement submitButtonElement =  loginPage.GetElementBy(loginPage.loginBtn);
		submitButtonElement.click();

		//Make sure user is not Navigated
		String ActualPageUrl = loginPage.GetUrl();
		String ExpectedPageTitle = "https://www.vanmoof.com/my-vanmoof/auth/login";
		Assert.assertEquals(ExpectedPageTitle, ActualPageUrl);

		//Assert Message "These credentials do not match our records." is displayed
		loginPage.WaitForElement(loginPage.validationMessageforWrongCredentials);
		String actualValidationMessage = loginPage.GetElementBy(loginPage.validationMessageforWrongCredentials).getText();
		Assert.assertEquals("These credentials do not match our records.", actualValidationMessage);
	}
}