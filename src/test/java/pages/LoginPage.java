package pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class LoginPage {

    private final WebDriver driver;
    private final WebDriverWait wait;

    public LoginPage() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 60);
        openPage();
    }

    public By emailField = By.id("email");
    public By passwordField = By.id("password");
    public By loginBtn = By.xpath("//form[1]/button[1]");
    public By registerNewBikeBtn = By.xpath("//p[@class='chakra-text css-e38lj8']");
    public By createNewAccountBtn = By.xpath("//a[@class='chakra-button css-1c9pvxu']");

    //Ui Elements
    public By backToShopBtn = By.xpath("//a[@class='chakra-link css-1je5tvy']");
    public By newHereLabel = By.xpath("//div[2]/p[1]");
    public By withVanMoofAccountLabel = By.xpath("//p[@class='chakra-text css-1pt225d']");
    public By trackYourOrderLabel = By.xpath("//ul[@class='css-bhb3xr']/li[1]");
    public By forgotPasswordBtn = By.xpath("//a[@class='chakra-link css-17lquin']");
    public By supportBtn = By.xpath("//div[@class='chakra-stack css-1cehjsj']/a[1]");
    public By languageBtn = By.xpath("//a[@class='chakra-link css-eux14o']");
    public By barLoginBtn = By.xpath("//a[2]");
    public By bottomSentenceLabel_RideWith = By.xpath("//p[@class='b-outlet-sale__body-text']");
    public By bottomSentenceLabel_AnOFFER = By.xpath("//p[@class='b-outlet-sale__title-small']");
    public By shopOutletBtn = By.xpath("//span[@class='m-btn__text']");
    public By loginLabel = By.xpath("//p[@class='chakra-text css-1tf2uvh']");
    public By bookCheckinLabel = By.xpath("//ul[@class='css-bhb3xr']/li[3]");

    //Validation message
    public By validationMessageforWrongCredentials = By.xpath("//div[@class='chakra-alert css-zekgfs']");

    //Create New Account
    public By createNewAccountTitle = By.xpath("//h1[@class='chakra-heading css-1rv1x27']");

    //Reset password
    public By resetPasswordTitle = By.xpath("//h1[@class='chakra-heading css-1pwgd4t']");

    //Back to shop page
    public By rideTheFutureTogetherTitle = By.xpath("//p[@class='b-app-section__heading']");

    //Outlet page
    public By outletSalePageTitle = By.xpath("//*[@id='sub_smart_bikes']/div/div/h2[1]");

    //Support Page
    public By supportPageTitle = By.xpath("//h1[1]/strong[1]");

    private void openPage() {
        driver.get("https:\\www.vanmoof.com\\my-vanmoof\\auth\\login");
    }

    public WebElement GetElementBy(By by) { return driver.findElement(by);}
    public void WaitForElement(By by) { wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by)); }
    public String GetUrl() { return driver.getCurrentUrl();}
    public String GetPageTitle() { return driver.getTitle();}
    public boolean CheckIfTextExist(String s) {return driver.getPageSource().contains(toString());}

    public void VerifyTextPresent(String value) { driver.getPageSource().contains(value); }

    public void NavigateToNextTab () {
        List<String> browserTabs = new ArrayList(driver.getWindowHandles());
	    driver.switchTo().window(browserTabs.get(1));
    }

    public void cleanup() {
        if (driver != null)
            driver.quit();
    }
}
